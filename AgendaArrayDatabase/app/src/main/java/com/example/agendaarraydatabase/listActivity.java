package com.example.agendaarraydatabase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;

public class listActivity extends android.app.ListActivity {
    private AgendaContactos agendaContacto;
    private Button btnNuevo;
    private ArrayList<Contacto> listaContacto;
    private MyArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        btnNuevo = (Button) findViewById(R.id.btnNuevo);
        agendaContacto = new AgendaContactos(this);

        llenarLista();
        adapter = new MyArrayAdapter(this, R.layout.layout_contacto, listaContacto);
        String str = adapter.contactos.get(1).getNombre();
        setListAdapter(adapter);

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void llenarLista(){
        agendaContacto.openDataBase();
        listaContacto = agendaContacto.allContactos();
        agendaContacto.cerrar();
    }

    class MyArrayAdapter extends ArrayAdapter<Contacto>{
        private Context context;
        private int textViewResourceId;
        private ArrayList<Contacto> contactos;

        public MyArrayAdapter(@NonNull Context context, int resource, ArrayList<Contacto> contactos){
            super(context, resource);
            this.context = context;
            this.textViewResourceId = resource;
            this.contactos = contactos;
        }

        public View getView(final int position, View converView, ViewGroup viewGroup){
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);
            TextView lblNombre = view.findViewById(R.id.lblNombreContacto);
            TextView lblTelefono = view.findViewById(R.id.lblTelefonoContacto);

            Button btnModificar = view.findViewById(R.id.btnModificar);
            Button btnBorrar = view.findViewById(R.id.btnBorrar);

            if(contactos.get(position).getFavorito()>0){
                lblNombre.setTextColor(Color.BLUE);
                lblTelefono.setTextColor(Color.BLUE);
            }
            else{
                lblNombre.setTextColor(Color.BLACK);
                lblTelefono.setTextColor(Color.BLACK);
            }
            lblNombre.setText(contactos.get(position).getNombre());
            lblTelefono.setText(contactos.get(position).getNombre());

            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    agendaContacto.openDataBase();
                    agendaContacto.deleteContacto(contactos.get(position).get_ID());
                    agendaContacto.cerrar();
                    notifyDataSetChanged();
                }
            });

            btnModificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("contacto", (Serializable) contactos.get(position));
                    Intent i = new Intent();
                    i.putExtras(bundle);
                    setResult(Activity.RESULT_OK);
                    finish();
                }
            });
            return view;
        }
    }
}
