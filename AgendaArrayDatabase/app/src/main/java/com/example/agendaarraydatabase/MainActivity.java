package com.example.agendaarraydatabase;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private AgendaContactos agendaContactos;

    private long id;
    private AgendaContactos db;
    private Contacto saveContacto;

    private EditText edtNombre;
    private EditText edtTelefono;
    private EditText edtTelefono2;
    private EditText edtDireccion;
    private EditText edtNotas;
    private CheckBox cbxFavorito;

    private Button btnGuardar;
    private Button btnListar;
    private Button btnLimpiar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNombre = findViewById(R.id.txtNombre);
        edtTelefono = findViewById(R.id.txtTel1);
        edtTelefono2 = findViewById(R.id.txtTel2);
        edtDireccion = findViewById(R.id.txtDomicilio);
        edtNotas = findViewById(R.id.txtNota);

        cbxFavorito = findViewById(R.id.chkFavorito);

        btnGuardar = findViewById(R.id.btnGuardar);
        btnListar = findViewById(R.id.btnListar);
        btnLimpiar = findViewById(R.id.btnLimpiar);

        db = new AgendaContactos(MainActivity.this);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtNombre.getText().toString().matches("") || edtTelefono.getText().toString().matches("") || edtDireccion.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this, "Favor de rellenar los campos", Toast.LENGTH_SHORT).show();
                }
                else{
                    Contacto c = new Contacto();
                    c.setNombre(edtNombre.getText().toString());
                    c.setTelefono1(edtTelefono.getText().toString());
                    c.setTelefono2(edtTelefono2.getText().toString());
                    c.setDomicilio(edtDireccion.getText().toString());
                    c.setNotas(edtNotas.getText().toString());
                    c.setFavorito(cbxFavorito.isChecked()? 1:0);
                    db.openDataBase();
                    if(saveContacto == null){
                        id = db.insertarContacto(c);
                        Toast.makeText(MainActivity.this, "El contacto se ha guardado.", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        db.updateContacto(c, id);
                        Toast.makeText(MainActivity.this, "El contacto se ha actualizado", Toast. LENGTH_SHORT).show();
                    }
                    db.cerrar();
                    limpiar();
                }
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ListActivity.class);
                startActivityForResult(i,0);
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
    }

    private void limpiar(){
        edtNombre.setText("");
        edtTelefono.setText("");
        edtTelefono2.setText("");
        edtDireccion.setText("");
        edtNotas.setText("");
        cbxFavorito.setChecked(false);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(Activity.RESULT_OK == resultCode){
            Contacto contacto = (Contacto) data.getSerializableExtra("contacto");
            saveContacto = contacto;
            id = contacto.get_ID();
            edtNombre.setText(contacto.getNombre());
            edtTelefono.setText(contacto.getTelefono1());
            edtTelefono2.setText(contacto.getTelefono2());
            edtDireccion.setText(contacto.getDomicilio());
            edtNotas.setText(contacto.getNotas());
            if(contacto.getFavorito() > 0){
                cbxFavorito.setChecked(true);
            }
        }
        else{
            limpiar();
        }
    }
}
